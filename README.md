# Projeto Integrador - Coleta e analise de sentimento no Twitter
## Twesis

**Grupo de desenvolvimento:**

- [Allan Takeuchi Bustamante](https://gitlab.com/allantak)
- [Henrique Canteiro de Oliveira](https://gitlab.com/Creaper001)
- [Wiliam Menezes Depiro](https://gitlab.com/wiliammd)

**Resumo:**

Esse é um projeto integrador criado por alunos do 1º termo de big data no agronegócio na Fatec de Pompeia com a função de obter tweets através da API do Twitter, e após armazena-los em CSV, com o auxilio de machine learning, fazer uma analise de sentimentos (Positivos e Negativos), gerando gráficos com porcentagens e datas das coletas.

**Documentações:**

- [Arquivos](../Arquivos)
- [Colab](../Colab)
- [Cronograma](../Documentação/Cronograma.xlsx)
- [Introdução](../Documentação/Introdução.pptx)
- [MVP](../Documentação/MVP.pptx)
- [Mapa do Conhecimento](../Documentação/Mapa do Conhecimento.jpg)
- [Matriz de Habilidade](../Documentação/Matriz de Habilidades.xlsx)
- [Relatorio](../Documentação/Relatorio.docx)

**Video de Apresentação**

- [https://www.youtube.com/watch?v=8NdOR3Atf6Q](https://www.youtube.com/watch?v=8NdOR3Atf6Q)

**Bibliotecas:**

- tweepy
- pandas
- numpy
- json
- CSV
- sklearn
- matplotlib
- datetime, time
- nltk
- re
- os.path

**Funcionamento:**

O código esta dividido em classes, com cada uma tendo funções especificas:

- "Cursomer().tweets(nomeArquivo, palavrasChave, filtrarRetweets)": Coleta ultimos 1500 tweets
- "Streamer().stream(nomeArquivo, palavrasChave, filtrarRetweets)": Coleta tweets até o programa ser encerrado
- "Csv(nomeArquivo)": Retorna valores e graficos dos arquivos CSV
    - ".dates()": Retorna gráfico de datas
    - ".fellings()": Retorana gráficos de sentimentos
    - ".dataFrame()": Retorna CSV como DataFrame
- "Analizer().sentiment(nomeArquivo)": Identifica sentimentos em arquivos CSV criados
- "Analizer().test(frasesTeste)": Testa resultados da analise de sentimentos

Ps: É necessário colocar suas chaves de acesso para a API do Twitter em "authentication()".