import keys as key
from tweepy import OAuthHandler
from tweepy import Stream
from tweepy.streaming import StreamListener
from tweepy import API
from tweepy import Cursor
import json
import csv
import pandas as pd
import numpy as np
import time
import re
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import os.path
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB
import nltk
class Analizer():
    def __init__(self):
        nltk.download('rslp')#
        self.df = pd.read_csv('Training_Tweets.csv', encoding='utf-8')
        self.df.drop_duplicates(inplace=True)
        self.vectorizer = CountVectorizer(analyzer="word")
        freq_tweets = self.vectorizer.fit_transform(self.df['Text'].values.astype('U'))
        self.model = MultinomialNB()
        self.model.fit(freq_tweets, self.df['Felling'])
    def preprocessing(self, tweet):
        stemmer = nltk.stem.RSLPStemmer()
        tweet = re.sub(r"http\S+", "", tweet).lower()
        word = [stemmer.stem(i) for i in tweet.split()]#
        return (" ".join(word))#
    def test(self, tests):
        texts = [self.preprocessing(i) for i in tests]
        freq_tweets = self.vectorizer.transform(texts)
        for i, j, p in zip(texts, self.model.predict(freq_tweets), self.model.predict_proba(freq_tweets).round(2)):
            print('Text:', i, 'Felling:', j, 'Proba: ', p)
        return True
    def sentiment(self, fileName):
        fileName = 'Tweets/'+fileName+'.csv'
        df = pd.read_csv(fileName, encoding='utf-8')
        df.drop_duplicates(inplace=True)
        texts = [self.preprocessing(i) for i in df['Text']]
        freq_tweets = self.vectorizer.transform(texts)
        df['Felling'] = self.model.predict(freq_tweets)
        with open(fileName, 'w', newline='', encoding='utf-8') as f:
            df.to_csv(f, mode='w', index=False, encoding='utf-8')
        return True
def authentication():
    auth = OAuthHandler(key.OAuthHandler1, key.OAuthHandler2)
    auth.set_access_token(key.set_access_token1, key.set_access_token2)
    return auth
class Csv():
    def __init__(self, fileName):
        self.df = pd.read_csv('Tweets/'+fileName+'.csv', encoding='utf-8')
        self.df.drop_duplicates(inplace=True)
        self.fileName = fileName
    def drop_duplicates(self):
        pass
    def dataFrame(self):
        return self.df
    def fellings(self):
        self.df['Felling'].value_counts().plot(kind='bar')
        plt.show()
        return self.df['Felling'].value_counts()
    def dates(self):
        df = pd.DataFrame(data=[datetime.strptime(i, '%Y-%m-%d, %H:%M:%S').strftime('%Y-%m-%d') for i in self.df['Date']], columns=['Date'])
        df = df['Date'].value_counts().rename_axis('Date').reset_index(name='Counts')
        df = df.sort_values(['Date'])
        plt.plot(df['Date'], df['Counts'], '--o')
        plt.title('Tweets Coletados')
        plt.xlabel('Datas')
        plt.ylabel('Quantidade')
        plt.show()
        return df['Date'].value_counts()
class Streamer():
    def __init__(self):
        self.auth = authentication()
    def stream(self, fileName, keys, filterRetweets):
        fileName = 'Tweets/'+fileName+'.csv'
        try:
            if(os.path.isfile(fileName) == False):
                with open(fileName, 'a', newline='') as f:
                    w = csv.writer(f)
                    w.writerow(['Tweet_Id', 'Text', 'Lang', 'Date', 'User_Id', 'User_Screen_Name', 'Place_Name', 'Felling'])
            listener = StdoutListener(fileName, filterRetweets)
            stream = Stream(self.auth, listener)
            stream.filter(track=keys, languages=['pt'])
        except BaseException as e:
            print(e)
            return e
class StdoutListener(StreamListener):
    def __init__(self, fileName, filterRetweets):
        self.fileName = fileName
        self.filterRetweets = filterRetweets
    def on_data(self, data):
        try:
            data = json.loads(data)
            if(not hasattr(data, 'retweeted_status') or not self.filterRetweets):
                if(not hasattr(data, 'place')): place = 'None'
                else: place = data['place']['full_name']
                text = " ".join(data['extended_tweet']['full_text'].splitlines())
                felling = False
                time = datetime.strptime(data['created_at'], '%a %b %d %H:%M:%S +0000 %Y')
                time = time - timedelta(hours=3)
                date = time.strftime('%Y-%m-%d, %H:%M:%S')
                tweet = [data['id'], text, data['lang'], date, data['user']['id'], data['user']['screen_name'], place, felling]
                with open(self.fileName, 'a', newline='', encoding='utf-8') as f:
                    w = csv.writer(f)
                    w.writerow(tweet)
            return True
        except BaseException as e:
            return e
        return False
    def on_error(self, status):
        if(status == 420):
            return status
        return False
class Cursomer():
    def __init__(self):
        self.auth = authentication()
        self.api = API(self.auth)
    def tweets(self, fileName, keys, filterRetweets):
        fileName = 'Tweets/'+fileName+'.csv'
        c = ''
        for key in keys:
          c = c+' '+key
        keys = c
        if(filterRetweets): keys = keys+' -filter:retweets'
        if(os.path.isfile(fileName) == False):
            with open(fileName, 'a', newline='') as f:
                w = csv.writer(f)
                w.writerow(['Tweet_Id', 'Text', 'Lang', 'Date', 'User_Id', 'User_Screen_Name', 'Place_Name', 'Felling'])
        try:
            for data in Cursor(self.api.search, tweet_mode='extended', q=keys, lang='pt').items(1500):
                if(data.place == None): place = 'None'
                else: place = data.place.full_name
                text = " ".join(data.full_text.splitlines())
                felling = False
                time = data.created_at - timedelta(hours=3)
                date = time.strftime('%Y-%m-%d, %H:%M:%S')
                tweet = [data.id, text, data.lang, date, data.user.id, data.user.screen_name, place, felling]
                with open(fileName, 'a', newline='', encoding='utf-8') as f:
                    w = csv.writer(f)
                    w.writerow(tweet)
            return True
        except BaseException as e:
            print(e)
            return e
if(__name__ == "__main__"):
    fileName = 'Tweets_Bolsonaro'
    keys = ['bolsonaro', 'presidente']

    print('Start:')

    #Cursomer().tweets(fileName, keys, True)

    #Streamer().stream(fileName, keys, True)

    #Analizer().sentiment(fileName)

    """
    tests = []
    Analizer().test(tests)
    """

    #print(Csv(fileName).dates())

    #print(Csv(fileName).fellings())

    #print(Csv(fileName).dataFrame())
    
    print('End.')